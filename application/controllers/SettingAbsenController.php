<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SettingAbsenController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$model = array('AbsenModel');
		$helper = array('tgl_indo','nominal');
		$this->load->model($model);
		$this->load->helper($helper);

		// cek status login
		if (!$this->session->has_userdata('session_id')) {
			$this->session->set_flashdata('alert', 'belum_login');
			redirect(base_url('login'));
		}
	}

    public function index()
	{
		$data = array(
			'setting_absen' => $this->AbsenModel->lihat_setting_absen(),
			'title' => 'Setting Absen'
		);
		$this->load->view('templates/header',$data);
		$this->load->view('backend/settingabsen/index', $data);
		$this->load->view('templates/footer');
	}

	public function updateForm($id){
		$data = $this->AbsenModel->lihat_setting_absen_by_id($id);
		echo json_encode($data);
	}

	public function update(){
		if (isset($_POST['update'])){
			$id = $this->input->post('id');
			$jam_masuk = $this->input->post('jam_masuk');
			$jam_keluar = $this->input->post('jam_keluar');
			$data = array(
				'jam_masuk' => $jam_masuk,
				'jam_keluar' => $jam_keluar,
			);
			$save = $this->AbsenModel->update_setting_absen($id,$data);
			if ($save>0){
				$this->session->set_flashdata('alert', 'update_setting_absen');
				redirect('settingabsen');
			}
			else{
				redirect('settingabsen');
			}
		}
	}
}