<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GajiController extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$model = array('GajiModel','PinjamModel');
		$helper = array('tgl_indo','nominal');
		$this->load->model($model);
		$this->load->helper($helper);
		if (!$this->session->has_userdata('session_id')) {
			$this->session->set_flashdata('alert', 'belum_login');
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'gaji' => $this->GajiModel->getlihat_gaji(),
			'title' => 'Gaji'
		);

		$this->load->view('templates/header',$data);
		$this->load->view('backend/gaji/index',$data);
		$this->load->view('templates/footer');
	}

	public function detail($id){
		$datagaji = $this->GajiModel->lihat_gaji_perorang($id);
 
		if(count($datagaji)>0){
			$data = array(
				'gaji' => $datagaji,
				'title' => 'Gaji'
			);
		}else{
			$data = array(
				'gaji' => array(),
				'title' => 'Gaji'
			);
		}

		// var_dump($data);

		$this->load->view('templates/header',$data);
		$this->load->view('backend/gaji/detail',$data);
		$this->load->view('templates/footer');
	}

	function getdataperhitungan($id){
		$data = $this->GajiModel->getdataperhitungan($id);
		echo json_encode($data);
	}


	public function lihat($id){
		$data = $this->GajiModel->lihat_satu_gaji_by_idv1($id);
		$data['jumlah_hari'] = $this->GajiModel->lihat_hari_kerja($data['karyawan_id']);

		echo json_encode($data);
	}

	public function pinjam($id){
		$data = $this->GajiModel->lihat_satu_gaji_pinjam($id);

		echo json_encode($data);
	}

	public function bayar($id){
		$data = array(
			'gaji_status' => 'sudah'
		);

		$save = $this->GajiModel->update_gaji($id,$data);

		if ($save>0){
			$gaji = $this->GajiModel->lihat_satu_gaji_pinjam($id);
			if ($gaji != null){
				if (($gaji['pinjam_jumlah'] - $gaji['pinjam_bayar']) > 500000){
					$dataGaji = array(
						'gaji_bayar_pinjaman' => 500000
					);
					$this->GajiModel->update_gaji($id,$dataGaji);
					$dataPinjam = array(
						'pinjam_bayar' => $gaji['pinjam_bayar'] + 500000
					);
					$this->PinjamModel->update_pinjaman($gaji['pinjam_id'],$dataPinjam);
				}else{
					$bayar = $gaji['pinjam_jumlah'] - $gaji['pinjam_bayar'];
					$dataGaji = array(
						'gaji_bayar_pinjaman' => $bayar
					);
					$this->GajiModel->update_gaji($id,$dataGaji);
					$dataPinjam = array(
						'pinjam_bayar' => $gaji['pinjam_bayar'] + $bayar
					);
					$this->PinjamModel->update_pinjaman($gaji['pinjam_id'],$dataPinjam);
				}
			}
			$this->session->set_flashdata('alert', 'update_gaji');
			redirect('gaji');
		}
		else{
			redirect('gaji');
		}
	}

	function pphsave(){
		if (isset($_POST['save'])) {
			$generate = substr(time(), 5);
			$idpph = 'PPH-'.$generate;
			$idkaryawan = $this->input->post('idkaryawan');
			$status_karyawan = $this->input->post('status_karyawan');
			$nama_karyawan = $this->input->post('nama_karyawan');
			$jabatan_karyawan = $this->input->post('jabatan_karyawan');
			$gaji_pokok = $this->input->post('gaji_pokok');
			$biaya_jabatan = $this->input->post('biaya_jabatan');
			$iuranpensiun = $this->input->post('iuranpensiun');
			$totalpengurang = $this->input->post('totalpengurang');
			$netoperbulan = $this->input->post('netoperbulan');
			$netoperpertahun = $this->input->post('netoperpertahun');
			$anak = $this->input->post('anak');
			$totalptkp = $this->input->post('totalptkp');
			$tkp = $this->input->post('tkp');
			$kenatahun = $this->input->post('kenatahun');
			$kenabulan = $this->input->post('kenabulan');

			$datakaryawan = explode(" | ", $nama_karyawan);

			$data = array(
				'pph_idpph' => $idpph,
				'pph_karyawan_id' => $datakaryawan[0], 
				'pph_karyawan_status' => $datakaryawan[1],
				'pph_iuranpensiun' => $iuranpensiun,
				'pph_totalpengurang' => $totalpengurang,
				'pph_noteperbulan' => $netoperbulan,
				'pph_notepertahun' => $netoperpertahun,
				'pph_anak' => $anak,
				'pph_totalptkp' => $totalptkp,
				'pph_tkp' => $tkp,
				'pph_kenatahun' => $kenatahun,
				'pph_kenabulan' => $kenabulan,
			);

			$save = $this->GajiModel->tambah_pph($data);

			if ($save > 0) {
				$this->session->set_flashdata('alert', 'pph_tamnbah');
				redirect('gaji');
			} else {
				redirect('gaji');
			}

		}
	}
}
