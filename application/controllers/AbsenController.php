<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AbsenController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$model = array('KaryawanModel', 'AbsenModel', 'GajiModel');
		$helper = array('tgl_indo_helper');
		$this->load->model($model);
		$this->load->helper($helper);

		// cek status login
		// if (!$this->session->has_userdata('session_id')) {
		// 	$this->session->set_flashdata('alert', 'belum_login');
		// 	redirect(base_url('login'));
		// }
	}

	public function index()
	{
		$data = array(
			'absen' => $this->AbsenModel->lihat_absen(),
			'karyawan' => json_encode($this->KaryawanModel->lihat_karyawan()),
			'title' => 'Absen'
		);
		$this->load->view('templates/header',$data);
		$this->load->view('backend/absen/index', $data);
		$this->load->view('templates/footer');
	}

	public function tambah()
	{
		if (isset($_POST['simpan'])) {
			$generate = substr(time(), 5);
			$id = 'ABS-' . $generate;
			$nama = $this->input->post('nama_karyawan');
			$status_absen = $this->input->post('status_absen');
			
			$hari = date ("D");

			switch($hari){
				case 'Sun':
					$sethari = "Minggu";
				break;
		 
				case 'Mon':			
					$sethari = "Senin";
				break;
		 
				case 'Tue':
					$sethari = "Selasa";
				break;
		 
				case 'Wed':
					$sethari = "Rabu";
				break;
		 
				case 'Thu':
					$sethari = "Kamis";
				break;
		 
				case 'Fri':
					$sethari = "Jumat";
				break;
		 
				case 'Sat':
					$sethari = "Sabtu";
				break;
				
				default:
					$sethari = "Tidak di ketahui";		
				break;
			}	

			$tglabsen= date('Y-m-d');
			$getnohari = $this->AbsenModel->getharike("absen_karyawan_id='$nama' AND absen_date_created LIKE '%$tglabsen%'")->row('hari_absen');

			$data = array(
				'absen_id' => $id,
				'absen_ke' => $getnohari, 
				'absen_karyawan_id' => $nama,
				'absen_in' => date('Y-m-d h:i:s'),
				'absen_hari' => $sethari,
			);

			$dataout = array(
				'absen_out' => date('Y-m-d h:i:s'),
			);

			$cekAbsen = $this->AbsenModel->cek_absen($nama, date('Y-m-d'));
			if ($cekAbsen == null AND $status_absen == 'MASUK') {

				$gaji = $this->GajiModel->lihat_satu_gaji($nama);
				$karyawan = $this->KaryawanModel->lihat_satu_karyawan($nama);
				$tgl = explode('-', $karyawan['karyawan_tanggal_gabung']);
				$tglgabung = $tgl[2];

				$karyawanId = $nama;
				$gajiTotal = $karyawan['jabatan_gaji'] + $karyawan['jabatan_tunjab'] + $karyawan['jabatan_tunkon'] + $karyawan['jabatan_tunlan'];
				$bulan = $gaji['gaji_bulan_ke'];

				if ($gaji == null) {
					$generate = substr(time(), 5);
					$gajiId = 'GJI-' . $generate;
					$gajiTotal = $karyawan['jabatan_gaji'] + $karyawan['jabatan_tunjab'] + $karyawan['jabatan_tunkon'] + $karyawan['jabatan_tunlan'];

					$dataGaji = array(
						'gaji_id' => $gajiId,
						'gaji_karyawan_id' => $karyawanId,
						'gaji_total' => $gajiTotal,
						'gaji_tanggal' => date('Y-m-d'),
						'gaji_bulan_ke' => 1
					);
					$saveGaji = $this->GajiModel->tambah_gaji($dataGaji);
				} else {
					if (date('d') == $tglgabung) {
						$generate = substr(time(), 5);
						$gajiId = 'GJI-' . $generate;
						$gajiTotal = $karyawan['jabatan_gaji'] + $karyawan['jabatan_tunjab'] + $karyawan['jabatan_tunkon'] + $karyawan['jabatan_tunlan'];
						$bulan = $bulan + 1;

						$dataGaji = array(
							'gaji_id' => $gajiId,
							'gaji_karyawan_id' => $karyawanId,
							// 'gaji_total' => $gajiTotal,
							'gaji_total' => 123,
							'gaji_tanggal' => date('Y-m-d'),
							'gaji_bulan_ke' => $bulan
						);
						// $saveGaji = $this->GajiModel->tambah_gaji($dataGaji);
					} else {
						$gajiId = $gaji['gaji_id'];
						$gajiTotal = $gajiTotal + $gaji['gaji_total'];
						$dataGaji = array(
							'gaji_total' => $gajiTotal,
							'gaji_tanggal' => date('Y-m-d')
						);
						$updateGaji = $this->GajiModel->update_gaji($gajiId, $dataGaji);
					}
				}

				$save = $this->AbsenModel->tambah_absen($data);

				if ($save > 0) {
					$this->session->set_flashdata('alert', 'tambah_absen');
					redirect('absen');
				} else {
					redirect('absen');
				}
			
			} else if ($cekAbsen != null AND $status_absen == 'PULANG'){

				$wheredata= "absen_karyawan_id='$nama' AND absen_date_created LIKE '%$tglabsen%'";
				$id = $this->AbsenModel->cek_absen_pulang($wheredata)->row('absen_id'); 

				$save = $this->AbsenModel->tambah_absen_pulang($id,$dataout);

				if ($save) {
					$this->session->set_flashdata('alert', 'tambah_absen');
					redirect('absen');
				} else {
					redirect('absen');
				}

			} else {
				$this->session->set_flashdata('alert', 'absen_sudah_ada');
				redirect('absen');
			}
		}
	}

	public function lembur($id){
		$dataAbsen = array(
			'absen_status' => 'lembur',
			'absen_lembur' => date("Y-m-d 20:00:00")
		);

		$updateAbsen = $this->AbsenModel->update_absen($id,$dataAbsen);
		if ($updateAbsen > 0) {
			$cekAbsen = $this->AbsenModel->lihat_satu_absen($id);

			$gaji = $this->GajiModel->lihat_satu_gaji($cekAbsen['karyawan_id']);
			$gajiId = $gaji['gaji_id'];
			$gajiLembur = $gaji['gaji_lembur'];
			$gajiLembur = $gajiLembur + $gaji['jabatan_gaji'];
			$dataGaji = array(
				'gaji_lembur' => $gajiLembur
			);
			$updateGaji = $this->GajiModel->update_gaji($gajiId,$dataGaji);

			$this->session->set_flashdata('alert', 'update_absen');
			redirect('absen');
		} else {
			redirect('absen');
		}
	}

	public function izinMasuk($id){
		$dataAbsen = array(
			'absen_izin_masuk' => date("Y-m-d H:i:s")
		);

		$updateAbsenIzinMasuk = $this->AbsenModel->update_absen($id,$dataAbsen);
		if ($updateAbsenIzinMasuk > 0) {
			$this->session->set_flashdata('alert', 'update_absen');
			redirect('absen');
		} else {
			redirect('absen');
		}
	}

	public function izinkeluar($id){
		$dataAbsen = array(
			'absen_izin_keluar' => date("Y-m-d H:i:s")
		);

		$updateAbsenIzinKeluar = $this->AbsenModel->update_absen($id,$dataAbsen);
		if ($updateAbsenIzinKeluar > 0) {
			$this->session->set_flashdata('alert', 'update_absen');
			redirect('absen');
		} else {
			redirect('absen');
		}
	}

	// New Function Absen
	public function addabsen() {
		$id = $this->input->get('params', true);
		$hari = date ("D");
		$tglabsen= date('Y-m-d');

		switch($hari){
			case 'Sun':
				$sethari = "Minggu";
			break;
	 
			case 'Mon':			
				$sethari = "Senin";
			break;
	 
			case 'Tue':
				$sethari = "Selasa";
			break;
	 
			case 'Wed':
				$sethari = "Rabu";
			break;
	 
			case 'Thu':
				$sethari = "Kamis";
			break;
	 
			case 'Fri':
				$sethari = "Jumat";
			break;
	 
			case 'Sat':
				$sethari = "Sabtu";
			break;
			
			default:
				$sethari = "Tidak di ketahui";		
			break;
		}

		$response = array();

		$validasiKaryawan = $this->AbsenModel->check_karyawan($id);
		$lihatsettingabsen = $this->AbsenModel->lihat_setting_absen();
		$resultCheckkaryawan = $validasiKaryawan->num_rows();
		
		if ($resultCheckkaryawan > 0) {
			$dataKaryawan = $this->KaryawanModel->lihat_satu_karyawan($id);
			$generate = substr(time(), 5);
			$kode = 'ABS-' . $generate;
						
			$getnohari = $this->AbsenModel->getharike("absen_karyawan_id='$id' AND absen_date_created LIKE '%$tglabsen%'")->row('hari_absen');

			$data = array(
				'absen_id' => $kode,
				'absen_ke' => $getnohari, 
				'absen_karyawan_id' => $id,
				'absen_in' => date('Y-m-d h:i:s'),
				'absen_hari' => $sethari,
			);

			$dataout = array(
				'absen_out' => date('Y-m-d h:i:s'),
			);

			$cekAbsen = $this->AbsenModel->cek_absen($id, date('Y-m-d'));

			if ($cekAbsen == null) {
				$gaji = $this->GajiModel->lihat_satu_gaji($id);
				$karyawan = $this->KaryawanModel->lihat_satu_karyawan($id);
				$tgl = explode('-', $karyawan['karyawan_tanggal_gabung']);
				$tglgabung = $tgl[2];

				$karyawanId = $id;
				$gajiTotal = $karyawan['jabatan_gaji'] + $karyawan['jabatan_tunjab'] + $karyawan['jabatan_tunkon'] + $karyawan['jabatan_tunlan'];
				
				if ($gaji == null) {
					$bulan = 1;
				} else {
					$bulan = $gaji['gaji_bulan_ke'];
				}

				if ($gaji == null) {
					$generate = substr(time(), 5);
					$gajiId = 'GJI-' . $generate;
					$gajiTotal = $karyawan['jabatan_gaji'] + $karyawan['jabatan_tunjab'] + $karyawan['jabatan_tunkon'] + $karyawan['jabatan_tunlan'];
	
					$dataGaji = array(
						'gaji_id' => $gajiId,
						'gaji_karyawan_id' => $karyawanId,
						'gaji_total' => $gajiTotal,
						'gaji_tanggal' => date('Y-m-d'),
						'gaji_bulan_ke' => 1
					);
					$saveGaji = $this->GajiModel->tambah_gaji($dataGaji);
				} else {
					if (date('d') == $tglgabung) {
						$generate = substr(time(), 5);
						$gajiId = 'GJI-' . $generate;
						$gajiTotal = $karyawan['jabatan_gaji'] + $karyawan['jabatan_tunjab'] + $karyawan['jabatan_tunkon'] + $karyawan['jabatan_tunlan'];
						$bulan = $bulan + 1;
	
						$dataGaji = array(
							'gaji_id' => $gajiId,
							'gaji_karyawan_id' => $karyawanId,
							// 'gaji_total' => $gajiTotal,
							'gaji_total' => 123,
							'gaji_tanggal' => date('Y-m-d'),
							'gaji_bulan_ke' => $bulan
						);
						// $saveGaji = $this->GajiModel->tambah_gaji($dataGaji);
					} else {
						$gajiId = $gaji['gaji_id'];
						$gajiTotal = $gajiTotal + $gaji['gaji_total'];
						$dataGaji = array(
							'gaji_total' => $gajiTotal,
							'gaji_tanggal' => date('Y-m-d')
						);
						$updateGaji = $this->GajiModel->update_gaji($gajiId, $dataGaji);
					}
				}
	
				$save = $this->AbsenModel->tambah_absen($data);
				
				if ($save > 0) {
					$response['status'] = TRUE;	
					$response['message'] = 'Berhasil Menyinpan Absen Masuk ';
				} else {
					$response['status'] = FALSE;	
					$response['message'] = 'Gagal Menyimpan Absen Masuk';
				}

			}else if ($cekAbsen != null) {
				$wheredata= "absen_karyawan_id='$id' AND absen_date_created LIKE '%$tglabsen%'";
				$id = $this->AbsenModel->cek_absen_pulang($wheredata)->row('absen_id'); 

				$save = $this->AbsenModel->tambah_absen_pulang($id,$dataout);

				if ($save) {
					$response['status'] = TRUE;	
					$response['message'] = 'Berhasil Menyinpan Absen Pulang ';
				} else {
					$response['status'] = FALSE;	
					$response['message'] = 'Gagal Menyimpan Absen Pulang';
				}
			}else {
				$response['status'] = TRUE;	
				$response['message'] = 'Absen Sudah Ada';
			}
		}else {
			$response['status'] = FALSE;	
			$response['message'] = 'ID Karyawan Tidak Terdaftar';
		}

		echo json_encode($response);
	}
}
