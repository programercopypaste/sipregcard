<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanModel extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function lihat_laporan($tanggal){
		$this->db->select('*');
		$this->db->from('t_gaji');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->like('gaji_tanggal',$tanggal);
		$this->db->where('gaji_status','sudah');
		$this->db->order_by('gaji_bulan_ke','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}
}
