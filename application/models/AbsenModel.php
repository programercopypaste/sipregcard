<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AbsenModel extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function lihat_absen(){
		$this->db->select('*');
		$this->db->from('t_absen');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_absen.absen_karyawan_id');
		$this->db->order_by('absen_date_created','DESC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function lihat_satu_absen($id){
		$this->db->select('*');
		$this->db->from('t_absen');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_absen.absen_karyawan_id');
		$this->db->where('absen_id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function tambah_absen($data){
		$this->db->insert('t_absen', $data);
		return $this->db->affected_rows();
	}

	function tambah_absen_pulang($id,$data){
		$this->db->where('absen_id',$id)->update('t_absen',$data);
		return true;
	}

	function getharike($where){
		$this->db->select("if( SUM(absen_ke) IS NULL, 1, SUM(absen_ke) + 1) as hari_absen");
		$this->db->from("t_absen");
		$this->db->where($where);

		return $this->db->get();
	}

	public function cek_absen($id,$tanggal){
		$this->db->select('*');
		$this->db->from('t_absen');
		$this->db->where('absen_karyawan_id',$id);
		$this->db->like('absen_date_created',$tanggal);
		$query = $this->db->get();
		return $query->row_array();
	}

	function cek_absen_pulang($where){
		$this->db->select('*');
		$this->db->from("t_absen");
		$this->db->where($where);

		return $this->db->get();
	}

	public function update_absen($id,$data){
		$this->db->where('absen_id', $id);
		$this->db->update('t_absen', $data);
		return $this->db->affected_rows();
	}

	//Validasi Karyawan
	public function check_karyawan($id){
		$this->db->select('*');
		$this->db->from('t_karyawan');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->where('karyawan_id',$id);
		$query = $this->db->get();
		return $query;
	}

	// setting Absen
	public function lihat_setting_absen(){
		$this->db->select('*');
		$this->db->from('t_setting_absen');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function lihat_setting_absen_by_id($id){
		$this->db->select('*');
		$this->db->from('t_setting_absen');
		$this->db->where('setting_absen_id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function update_setting_absen($id, $data){
		$this->db->where('setting_absen_id', $id);
		$this->db->update('t_setting_absen', $data);
		return $this->db->affected_rows();
	}
}
