<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GajiModel extends CI_Model{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function lihat_gaji(){
		$this->db->select('*');
		$this->db->from('t_karyawan');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->order_by('karyawan_nama','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function lihat_hari_kerja($id) {
		$this->db->select('count(a.absen_id) as jumlah_hari');
		$this->db->from('t_absen a');
		$this->db->join('t_gaji b', 'a.absen_karyawan_id = b.gaji_karyawan_id');
		$this->db->where("a.absen_karyawan_id = '$id' AND b.gaji_status='belum'");
		$query = $this->db->get();
		return $query->row_array();
	}

	function getlihat_gaji(){
		$this->db->select('a.karyawan_id, a.karyawan_nama,b.jabatan_nama,
						   a.karyawan_tanggal_gabung,a.karyawan_nomor_hp,
						   if(c.absen_id IS NULL, 0, 1) AS cek_absen');
		$this->db->from('t_karyawan a');
		$this->db->join('t_jabatan b', 'a.karyawan_jabatan_id = b.jabatan_id', 'INNER');
		$this->db->join('t_absen c', 'a.karyawan_id = c.absen_karyawan_id', 'inner');
		$this->db->group_by('a.karyawan_id');
		$this->db->order_by('a.karyawan_nama','ASC');

		$query = $this->db->get();
		return $query->result_array();
	}

	function getdataperhitungan($id){
		$this->db->select("a.karyawan_id, a.karyawan_nama, a.karyawan_jabatan_id, b.jabatan_nama, c.gaji_total, a.karyawan_status");
		$this->db->FROM("t_karyawan a");
		$this->db->JOIN("t_jabatan b","a.karyawan_jabatan_id = b.jabatan_id", "INNER");
		$this->db->JOIN("t_gaji c","a.karyawan_id= c.gaji_karyawan_id","LEFT");
		$this->db->WHERE("a.karyawan_id='$id'");
		
		return $this->db->get()->result_array();

	}


	public function lihat_gaji_perorang($id){
		$this->db->select('*');
		$this->db->from('t_gaji');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->order_by('gaji_bulan_ke','DESC');
		$this->db->order_by('karyawan_nama','ASC');
		$this->db->where('gaji_karyawan_id',$id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function lihat_satu_gaji($id){
		$this->db->select('*');
		$this->db->from('t_gaji');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->where('gaji_karyawan_id',$id);
		$this->db->order_by('gaji_bulan_ke','DESC');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function lihat_satu_gaji_by_id($id){
		$this->db->select('*');
		$this->db->from('t_gaji');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
//		$this->db->join('t_pinjam', 't_pinjam.pinjam_karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->where('gaji_id',$id);
		$this->db->order_by('gaji_bulan_ke','DESC');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function lihat_satu_gaji_by_idv1($id){
		$this->db->select('*');
		$this->db->from('t_gaji');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->where('gaji_id',$id);
		$this->db->order_by('gaji_bulan_ke','DESC');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function lihat_satu_gaji_pinjam($id){
		$this->db->select('*');
		$this->db->from('t_gaji');
		$this->db->join('t_karyawan', 't_karyawan.karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->join('t_jabatan', 't_jabatan.jabatan_id = t_karyawan.karyawan_jabatan_id');
		$this->db->join('t_pinjam', 't_pinjam.pinjam_karyawan_id = t_gaji.gaji_karyawan_id');
		$this->db->where('gaji_id',$id);
		$this->db->order_by('gaji_bulan_ke','DESC');
		$query = $this->db->get();
		return $query->row_array();
	}

	public function lihat_pph($where){
		$this->db->select('*');
		$this->db->from('t_pph');
		$this->db->where($where);

		$query = $this->db->get();
		return $query;
	}

	public function tambah_gaji($data){
		$this->db->insert('t_gaji', $data);
		return $this->db->affected_rows();
	}

	public function update_gaji($id,$data){
		$this->db->where('gaji_id', $id);
		$this->db->update('t_gaji', $data);
		return $this->db->affected_rows();
	}

	function tambah_pph($data){
		// return $this->db->affected_rows();	
		return true;
	}
}
