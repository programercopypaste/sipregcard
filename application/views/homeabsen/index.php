<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SIPREG</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.css" id="theme-styles">
        <link rel="stylesheet" href="<?= base_url()?>assets/templateabsensi/css/style.css">
    </head>
    <body>
        <section class="banner">
            <div class="logo">
                <label
                    style="
                        display: block;
                        margin-top: 10px;
                        margin-bottom: 0px;
                        font-size: 100px;
                        font-weight: bold;
                    "
                >
                    SIPREG
                </label>  
                <label 
                    style="
                        display: block;
                        margin-top: -35px;
                        font-size: 20px;
                        font-weight: bold;
                    "
                >
                    Sistem Presensi Gaji Karyawan
                </label>          
                <h1 id="jam"></h1>
                <input style="
                        display: block; 
                        width: 30px; 
                        height: 30px; 
                        opacity: 0;
                    " 
                    type="text" 
                    id="idpegawai" 
                    name="idpegawai"
                >
                <img src="<?= base_url()?>assets/templateabsensi/img/2.png">
                <p>TAP YOUR CARD</p>
            </div>
        </section>
    </body>
</html>
<script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11/dist/sweetalert2.min.js"></script>
<script type="text/javascript">    
    window.onload = function() { jam(); }

    function set(e) {
        e = e < 10 ? '0'+ e : e;
        return e;
    }

    function jam() {
        var e = document.getElementById('jam'),
        d = new Date(), h, m, s;
        h = d.getHours();
        m = set(d.getMinutes());
        s = set(d.getSeconds());
    
        e.innerHTML = h +':'+ m +':'+ s;
    
        setTimeout('jam()', 1000);
    }

    function getIdPegawai(id) {
        $.ajax({
        url : "<?php echo site_url('absencontroller/addabsen')?>",
        type: "GET",
        data: { params: id },
        dataType: "JSON",
        success: function(e){
            var status = ''
            if(e.status == true) {
                status = 'success'
            } else {
                status = 'error'
            }

            messageAbsen(status, e.message)
        },
      });  
    }

    function messageAbsen(status, message) {
        Swal.fire({
            position: 'center',
            icon: status,
            title: message,
            showConfirmButton: false,
            timer: 1000
        })

        setTimeout(function() { 
            $('#idpegawai').val('')
            $('#idpegawai').focus();
            // window.location.reload()
        }, 1200);
    }

    $(document).ready(function(){
        $('#idpegawai').focus();

        $('#idpegawai').change(function() {
            setTimeout(function() { 
                var id = $('#idpegawai').val();
                getIdPegawai(id);
            }, 100);
        });
    });
</script>