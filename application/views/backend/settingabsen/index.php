<div class="row">
	<div class="col-md-12">
		<div class="card box-shadow-2">
			<?php
			if ($this->session->flashdata('alert') == 'tambah_setting_absen'):
				?>
				<div class="alert alert-success alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil ditambahkan
				</div>
			<?php
			elseif ($this->session->flashdata('alert') == 'update_setting_absen'):
				?>
				<div class="alert alert-success alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil diupdate
				</div>
			<?php
			elseif ($this->session->flashdata('alert') == 'hapus_setting_absen'):
				?>
				<div class="alert alert-danger alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil dihapus
				</div>
			<?php
			endif;
			?>
			<div class="card-header">
				<h1 style="text-align: center">Data Setting Absen</h1>
			</div>
			<hr>
			<div class="card-body">
				<table class="table table-bordered zero-configuration">
					<thead>
					<tr>
						<th>No</th>
						<th>Jam Masuk</th>
						<th>Jam Keluar</th>
						<td style="text-align: center"><i class="ft-settings spinner"></i></td>
					</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;

					foreach ($setting_absen as $key => $value):
						?>
						<tr>
							<td><?= $no ?></td>
							<td>
                                <?=
                                    $value['jam_masuk'];
                                ?>
                            </td>
                            <td>
                                <?=
                                    $value['jam_keluar'];
                                ?>
                            </td>
							<td>
								<?php if ($this->session->userdata('session_hak_akses') == 'manajer'):?>
								<button
									class="btn btn-success btn-sm  btn-bg-gradient-x-blue-green box-shadow-2"
									id="setting_absen_edit"
									data-toggle="modal" data-target="#ubah" value="<?= $value['setting_absen_id'] ?>"
									title="Update data setting absen">
									<i class="ft-edit"></i>
								</button>
								<?php endif;?>
							</td>
						</tr>
						<?php
						$no++;
					endforeach;
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal update -->
<div class="modal fade text-left" id="ubah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Update Setting Absen</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?= form_open('settingabsen/update') ?>
			<div class="modal-body">
				<fieldset class="form-group floating-label-form-group" hidden="true">
					<input type="text" class="form-control" id="setting_absen_id" name="id" autocomplete="off" required>
				</fieldset>
				
                <fieldset class="form-group floating-label-form-group">
					<label for="edit_nama">Jam Masuk (HH:mm:ss)</label>
					<input type="text" class="form-control" name="jam_masuk" id="jam_masuk" placeholder="Jam Masuk"
						   autocomplete="off" required>
				</fieldset>

                <fieldset class="form-group floating-label-form-group">
					<label for="edit_nama">Jam Keluar (HH:mm:ss)</label>
					<input type="text" class="form-control" name="jam_keluar" id="jam_keluar" placeholder="Jam Keluar"
						   autocomplete="off" required>
				</fieldset>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
					   value="Tutup">
				<input type="submit" class="btn btn-primary btn-bg-gradient-x-blue-cyan" name="update" value="Update">
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
<script>
	var root = window.location.origin + '/sipregcard/';

	$(document).ready(function(){
		$('#feedback').delay(3000).fadeOut('slow');

		$('#setting_absen_edit').click(function (e) {
			e.preventDefault();
			var id = $(this).val();
			var getUrl = root + 'settingabsen/updateForm/' + id;
			$.ajax({
				url : getUrl,
				type : 'ajax',
				dataType : 'json',
				success: function (response) {
					if (response != null){
						$('#setting_absen_id').val(id);
						$('#jam_masuk').val(response.jam_masuk);
						$('#jam_keluar').val(response.jam_keluar);
					}
				},
				error: function (response) {
					console.log(response.status + 'error');
				}
			});
		});
	});
</script>