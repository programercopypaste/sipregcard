<div class="row">
	<div class="col-md-12">
		<div class="card box-shadow-2">
			<?php
			if ($this->session->flashdata('alert') == 'tambah_karyawan'):
				?>
				<div class="alert alert-success alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil ditambahkan
				</div>
			<?php
			elseif ($this->session->flashdata('alert') == 'update_karyawan'):
				?>
				<div class="alert alert-success alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil diupdate
				</div>
			<?php
			elseif ($this->session->flashdata('alert') == 'hapus_karyawan'):
				?>
				<div class="alert alert-danger alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil dihapus
				</div>
			<?php
			endif;
			?>
			<div class="card-header">
				<h1 style="text-align: center">Data Karyawan</h1>
				<?php if ($this->session->userdata('session_hak_akses') == 'manajer'):?>
				<button type="button" class="btn btn-primary btn-bg-gradient-x-purple-blue box-shadow-2"
						data-toggle="modal" data-target="#bootstrap">
					<i class="ft-plus-circle"></i> Tambah data karyawan
				</button>
				<?php endif; ?>
			</div>
			<hr>
			<div class="card-body">
				<table class="table table-bordered zero-configuration">
					<thead>
					<tr>
						<th>No</th>
						<th>ID</th>
						<th>Nama</th>
						<th>Jabatan</th>
						<th>Tanggal Bergabung</th>
						<th>Nomor HP</th>
						<td style="text-align: center"><i class="ft-settings spinner"></i></td>
					</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;
					foreach ($karyawan as $key => $value):
						?>
						<tr>
							<td><?= $no ?></td>
							<td><?= $value['karyawan_id'] ?></td>
							<td><?= $value['karyawan_nama'] ?></td>
							<td><?= $value['jabatan_nama'] ?></td>
							<td><?= date_indo($value['karyawan_tanggal_gabung']) ?></td>
							<td><?= $value['karyawan_nomor_hp'] ?></td>
							<td>
								<button
									class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2"
									id="karyawan_lihat"
									data-toggle="modal" data-target="#lihat" value="<?= $value['karyawan_id'] ?>"
									title="Lihat selengkapnya">
									<i class="ft-eye"></i>
								</button>
								<?php if ($this->session->userdata('session_hak_akses') == 'manajer'):?>
								<button
									class="btn btn-success btn-sm  btn-bg-gradient-x-blue-green box-shadow-2"
									id="karyawan_edit"
									data-toggle="modal" data-target="#ubah" value="<?= $value['karyawan_id'] ?>"
									title="Update data karyawan">
									<i class="ft-edit"></i>
								</button>
								<button
									class="btn btn-danger btn-sm  btn-bg-gradient-x-red-pink box-shadow-2"
									id="karyawan_hapus"
									data-toggle="modal" data-target="#hapus" value="<?= $value['karyawan_id'] ?>"
									title="Hapus data karyawan">
									<i class="ft-trash"></i>
								</button>
								<?php endif;?>
							</td>
						</tr>
						<?php
						$no++;
					endforeach;
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal tambah -->
<div class="modal fade text-left" id="bootstrap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Tambah Data Karyawan</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?= form_open('karyawan/tambah') ?>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<fieldset class="form-group floating-label-form-group">
							<label for="nama">ID Karyawan</label>
							<input type="text" class="form-control" name="karyawan_id" id="karyawan_id" placeholder="ID Karyawan"
								   autocomplete="off" required>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="nama">Nama</label>
							<input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Karyawan"
								   autocomplete="off" required>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="tempat">Tempat Lahir</label>
							<input type="text" class="form-control" name="tempat_lahir" id="tempat" placeholder="Tempat Lahir"
								   autocomplete="off" required>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="tl">Tanggal Lahir</label>
							<div class='input-group'>
								<input type="date" class="form-control" name="tanggal_lahir" id="tl" placeholder="Tanggal Lahir"
									   autocomplete="off" required>
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="ft-calendar"></span>
									</span>
								</div>
							</div>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="alamat">Alamat</label>
							<textarea class="form-control" id="alamat" rows="3" name="alamat" placeholder="Alamat"
									  autocomplete="off" required></textarea>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="alamat">status</label>
		                     <?php echo form_dropdown('status_karyawan',array('MENIKAH'=>'MENIKAH', 'LAJANG'=>'LAJANG'),1,array('class'=>'form-control','id'=>'status_karyawan')) ?>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset class="form-group floating-label-form-group">
							<label for="tg">Tanggal Bergabung</label>
							<div class='input-group'>
								<input type="date" class="form-control" id="tg" name="tanggal_gabung"
									   placeholder="Tanggal Bergabung" autocomplete="off" required>
								<div class="input-group-append">
									<span class="input-group-text">
										<span class="ft-calendar"></span>
									</span>
								</div>
							</div>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="jabatan">Jabatan</label>
							<select name="jabatan" id="basicSelect" class="form-control">
								<?php
								foreach ($jabatan as $key => $value):
									?>
									<option value="<?= $value['jabatan_id'] ?>"><?= $value['jabatan_nama'] ?></option>
								<?php
								endforeach;
								?>
							</select>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="nohp">Nomor HP</label>
							<input type="number" class="form-control" id="nohp" name="nomor_hp" placeholder="Nomor HP"
								   autocomplete="off" required>
						</fieldset>
						<fieldset class="form-group floating-label-form-group">
							<label for="norek">Nomor Rekening</label>
							<input type="number" class="form-control" id="norek" name="nomor_rekening" placeholder="Nomor rekening boleh kosong"
								   autocomplete="off">
						</fieldset>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
					   value="Tutup">
				<input type="submit" class="btn btn-primary btn-bg-gradient-x-blue-cyan" name="simpan" value="Simpan">
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>


<!-- Modal lihat -->
<div class="modal fade text-left" id="lihat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Lihat Data Karyawan</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_karyawan_id">ID Karyawan</label>
					<input type="text" class="form-control" name="id" id="lihat_karyawan_id" placeholder="Nama Karyawan"
						autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_nama">Nama</label>
					<input type="text" class="form-control" name="nama" id="lihat_nama" placeholder="Nama Karyawan"
						autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_tempat">Tempat Lahir</label>
					<input type="text" class="form-control" name="tempat_lahir" id="lihat_tempat" value=""
						placeholder="Tempat Lahir" autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_tl">Tanggal Lahir</label>
					<div class='input-group'>
						<input type="date" class="form-control" name="tanggal_lahir" id="lihat_tl"
							   placeholder="Tanggal Lahir" autocomplete="off" readonly>
						<div class="input-group-append">
							<span class="input-group-text">
								<span class="ft-calendar"></span>
							</span>
						</div>
					</div>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_alamat">Alamat</label>
					<textarea class="form-control" id="lihat_alamat" rows="3" name="alamat" placeholder="Alamat"
							  autocomplete="off" readonly></textarea>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_tg">Tanggal Bergabung</label>
					<div class='input-group'>
						<input type="date" class="form-control" id="lihat_tg" name="tanggal_gabung"
							   placeholder="Tanggal Bergabung" autocomplete="off" readonly>
						<div class="input-group-append">
							<span class="input-group-text">
								<span class="ft-calendar"></span>
							</span>
						</div>
					</div>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_jabatan_karyawan">Jabatan</label>
					<input type="text" class="form-control" name="jabatan" id="lihat_jabatan_karyawan"
						placeholder="Jabatan" autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_gaji_pokok">Gaji Perhari</label>
					<input type="text" class="form-control" name="jabatan" id="lihat_gaji_pokok"
						placeholder="Gaji pokok" autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_nohp">Nomor HP</label>
					<input type="number" class="form-control" id="lihat_nohp" name="nomor_hp" placeholder="Nomor HP"
						autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="lihat_norek">Nomor Rekening</label>
					<input type="number" class="form-control" id="lihat_norek" name="nomor_rekening" placeholder="Nomor rekening boleh kosong"
						autocomplete="off" readonly>
				</fieldset>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal" value="Tutup">
			</div>
		</div>
	</div>
</div>


<!-- Modal update -->
<div class="modal fade text-left" id="ubah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Update Data Karyawan</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?= form_open('karyawan/update') ?>
			<div class="modal-body">
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_nama">ID Karyawan</label>
					<input type="text" class="form-control" id="edit_karyawan_id" name="id" autocomplete="off" required>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_nama">Nama</label>
					<input type="text" class="form-control" name="nama" id="edit_nama" placeholder="Nama Karyawan"
						   autocomplete="off" required>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_tempat">Tempat Lahir</label>
					<input type="text" class="form-control" name="tempat_lahir" id="edit_tempat" placeholder="Tempat Lahir"
						   autocomplete="off" required>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_tl">Tanggal Lahir</label>
					<div class='input-group'>
						<input type="date" class="form-control" name="tanggal_lahir" id="edit_tl" placeholder="Tanggal Lahir"
							   autocomplete="off" required>
						<div class="input-group-append">
										<span class="input-group-text">
											<span class="ft-calendar"></span>
										</span>
						</div>
					</div>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_alamat">Alamat</label>
					<textarea class="form-control" id="edit_alamat" rows="3" name="alamat" placeholder="Alamat"
							  autocomplete="off" required></textarea>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_tg">Tanggal Bergabung</label>
					<div class='input-group'>
						<input type="date" class="form-control" id="edit_tg" name="tanggal_gabung"
							   placeholder="Tanggal Bergabung" autocomplete="off" required>
						<div class="input-group-append">
							<span class="input-group-text">
								<span class="ft-calendar"></span>
							</span>
						</div>
					</div>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="jabatan">Jabatan</label>
					<select name="jabatan" id="jabatan" class="select2 form-control" style="width: 100%">
						<?php
						foreach ($jabatan as $key => $value):
							?>
							<option value="<?= $value['jabatan_id'] ?>"><?= $value['jabatan_nama'] ?></option>
						<?php
						endforeach;
						?>
					</select>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_nohp">Nomor HP</label>
					<input type="number" class="form-control" id="edit_nohp" name="nomor_hp" placeholder="Nomor HP"
						   autocomplete="off" required>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="edit_norek">Nomor Rekening</label>
					<input type="number" class="form-control" id="edit_norek" name="nomor_rekening" placeholder="Nomor rekening boleh kosong"
						   autocomplete="off">
				</fieldset>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
					   value="Tutup">
				<input type="submit" class="btn btn-primary btn-bg-gradient-x-blue-cyan" name="update" value="Update">
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>


<!-- Modal hapus -->
<div class="modal fade text-left" id="hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Hapus Data Karyawan ?</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-blue-cyan" data-dismiss="modal" value="Tutup">
				<div id="hapuskaryawan">

				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
<script>
	var root = window.location.origin + '/sipregcard/';
	var options = {
		url : root + 'karyawan/ajaxIndex',
		getValue: 'karyawan_nama',
		adjustWidth : false,
		list: {
			sort: {
				enabled: true
			},
			showAnimation: {
				type: "fade", //normal|slide|fade
				time: 400,
				callback: function() {}
			},

			hideAnimation: {
				type: "slide", //normal|slide|fade
				time: 400,
				callback: function() {}
			},
			match: {
				enabled: true
			},
			onSelectItemEvent: function() {
				var value = $("#nama_karyawan").getSelectedItemData().karyawan_id;

				$("#id_karyawan").val(value).trigger("change");
			}
		}
	};

	function formatRupiah(angka, prefix){
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if(ribuan){
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	$(document).ready(function(){
		$('#feedback').delay(3000).fadeOut('slow');

		$('#karyawan_lihat').click(function (e) {
			e.preventDefault();
			var id = $(this).val();
			var getUrl = root + 'karyawan/lihat/' + id;
			$.ajax({
				url : getUrl,
				type : 'ajax',
				dataType : 'json',
				success: function (response) {
					if (response != null){
						$('#lihat_karyawan_id').val(response.karyawan_id);
						$('#lihat_nama').val(response.karyawan_nama);
						$('#lihat_tempat').val(response.karyawan_tempat_lahir);
						$('#lihat_tl').val(response.karyawan_tanggal_lahir);
						$('#lihat_alamat').val(response.karyawan_alamat);
						$('#lihat_nohp').val(response.karyawan_nomor_hp);
						$('#lihat_norek').val(response.karyawan_no_rekening);
						$('#lihat_tg').val(response.karyawan_tanggal_gabung);
						$('#lihat_jabatan_karyawan').val(response.jabatan_nama);
						$('#lihat_gaji_pokok').val(formatRupiah(response.jabatan_gaji,'Rp. '));
					}
				},
				error: function (response) {
					console.log(response.status + 'error');
				}
			});
		});

		$('#karyawan_edit').click(function (e) {
			e.preventDefault();
			var id = $(this).val();
			var getUrl = root + 'karyawan/lihat/' + id;
			$.ajax({
				url : getUrl,
				type : 'ajax',
				dataType : 'json',
				success: function (response) {
					if (response != null){
						// $('#karyawan_id').val(response.karyawan_id);
						$('#edit_karyawan_id').val(id);
						$('#edit_nama').val(response.karyawan_nama);
						$('#edit_tempat').val(response.karyawan_tempat_lahir);
						$('#edit_tl').val(response.karyawan_tanggal_lahir);
						$('#edit_alamat').val(response.karyawan_alamat);
						$('#edit_nohp').val(response.karyawan_nomor_hp);
						$('#edit_norek').val(response.karyawan_no_rekening);
						$('#edit_tg').val(response.karyawan_tanggal_gabung);
						$('#edit_jabatan_karyawan').val(response.gaji_jabatan);
						$('#edit_gaji_pokok').val(formatRupiah(response.gaji_jumlah,'Rp. '));
						console.log(response);
					}
				},
				error: function (response) {
					console.log(response.status + 'error');
				}
			});
		});

		$('#karyawan_hapus').click(function () {
			var id = $(this).val();
			var html = '' +
				'<a href="'+root+'karyawan/hapus/'+id+'" class="btn btn-danger btn-bg-gradient-x-red-pink">Hapus</a>';
			$('#hapuskaryawan').html(html);
		});

		$('#nama_karyawan').easyAutocomplete(options);
	});
</script>