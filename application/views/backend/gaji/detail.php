<div class="row d-print-none">
	<div class="col-md-12">
		<div class="card box-shadow-2">

			<?php
			if ($this->session->flashdata('alert') == 'tambah_absen'):
				?>
				<div class="alert alert-success alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Berhasil absen
				</div>
			<?php
			elseif ($this->session->flashdata('alert') == 'update_gaji'):
				?>
				<div class="alert alert-success alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil diupdate
				</div>
			<?php
			elseif ($this->session->flashdata('alert') == 'hapus_absen'):
				?>
				<div class="alert alert-danger alert-dismissible animated fadeInDown" id="feedback" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Data berhasil dihapus
				</div>
			<?php
			endif;
			?>
			<div class="card-header">
				<h1 style="text-align: center">Detail Gaji <?=$gaji[0]['karyawan_nama']?></h1>
			</div>
			<div class="card-body">
				<table class="table table-bordered zero-configuration" style="width: 100%">
					<thead>
					<tr>
						<td style="width: 2%">No</td>
						<td>Nama Karyawan</td>
						<td>Jabatan</td>
						<td>Gaji Bulan Ini</td>
						<td>Bulan ke</td>
						<td>Status Bayar</td>
						<td style="text-align: center"><i class="ft-settings spinner"></i></td>
					</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;
					foreach ($gaji as $key => $value):
						?>
						<tr>
							<td><?= $no ?></td>
							<td><?= $value['karyawan_nama'] ?></td>
							<td><?= $value['jabatan_nama'] ?></td>
							<td>Rp. <?= nominal($value['gaji_lembur'] + $value['gaji_total']) ?></td>
							<td><?= $value['gaji_bulan_ke'] ?></td>
							<td>
								<?php
								if ($value['gaji_status'] == 'belum'):
									?>
									<div class="badge badge-warning"><i class="ft-x-circle"></i> Belum</div>
								<?php
								else:
									?>
									<div class="badge badge-success"><i class="ft-check-circle"></i> Sudah</div>
								<?php
								endif;
								?>
							</td>
							<td>
								<button
									class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-lihat"
									data-toggle="modal" data-target="#lihat" value="<?= $value['gaji_id'] ?>"
									title="Lihat selengkapnya"><i class="ft-eye"></i></button>
								<!-- <button
									class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 hitung_pajak"
									data-toggle="modal" data-target="#modal_hitung_pajak" value="<?= $value['karyawan_id'] ?>"
									title="Hitung Pajak"><i class="ft-credit-card"></i></button> -->
								<?php if ($this->session->userdata('session_hak_akses') == 'manajer'): ?>
									<?php
									if ($value['gaji_status'] == 'belum'):
										?>					
										<button
											class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-slip"
											data-toggle="modal" data-target="#slip" value="<?= $value['gaji_id'] ?>"
											title="Lihat slip gaji"><i class="ft-printer"></i></button>
										<button
											class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-bayar"
											data-toggle="modal" data-target="#bayar" value="<?= $value['gaji_id'] ?>"
											title="Sudah bayar ?"><i class="ft-check-square"></i></button>
									<?php
									else:
										?>

										<button
											class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-slip"
											data-toggle="modal" data-target="#slip" value="<?= $value['gaji_id'] ?>"
											title="Sudah bayar"><i class="ft-printer"></i></button>
										<button
											class="btn btn-success btn-sm  btn-bg-gradient-x-purple-blue box-shadow-2 gaji-bayar"
											data-toggle="modal" data-target="#bayar" value="<?= $value['gaji_id'] ?>"
											title="Sudah bayar" disabled><i class="ft-check-square"></i></button>
									<?php
									endif;
									?>
								<?php
								endif;
								?>
							</td>
						</tr>

						<?php
						$no++;
					endforeach;
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal lihat -->
<div class="modal fade text-left" id="lihat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Lihat Data Gaji</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_nama">Nama Karyawan</label>
					<input type="text" class="form-control" name="nama" id="gaji_lihat_nama" placeholder="Nama Karyawan"
						   autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_jabatan">Jabatan</label>
					<input type="text" class="form-control" name="tempat_lahir" id="gaji_lihat_jabatan" value=""
						   placeholder="Tempat Lahir" autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_tg">Tanggal Bergabung</label>
					<div class='input-group'>
						<input type="date" class="form-control" id="gaji_lihat_tg" name="tanggal_gabung"
							   placeholder="Tanggal Bergabung" autocomplete="off" readonly>
						<div class="input-group-append">
										<span class="input-group-text">
											<span class="ft-calendar"></span>
										</span>
						</div>
					</div>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_lembur">Gaji Lembur</label>
					<input type="text" class="form-control" name="jabatan" id="gaji_lihat_lembur"
						   placeholder="Gaji lembur" autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_gaji">Gaji Biasa</label>
					<input type="text" class="form-control" id="gaji_lihat_gaji" name="nomor_hp"
						   placeholder="Nomor HP"
						   autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_total">Total Gaji</label>
					<input type="text" class="form-control" id="gaji_lihat_total" name="nomor_rekening"
						   placeholder="Nomor rekening boleh kosong"
						   autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_total">Gaji Bersih</label>
					<input type="text" class="form-control" id="gaji_lihat_bersih" name="nomor_rekening"
						   placeholder="Nomor rekening boleh kosong"
						   autocomplete="off" readonly>
				</fieldset>
				<fieldset class="form-group floating-label-form-group">
					<label for="gaji_lihat_bulan">Bulan ke</label>
					<input type="number" class="form-control" id="gaji_lihat_bulan" name="nomor_rekening"
						   placeholder="Nomor rekening boleh kosong"
						   autocomplete="off" readonly>
				</fieldset>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
					   value="Tutup">
			</div>
		</div>
	</div>
</div>


<!-- Modal slip -->
<style type="text/css">
	.tengah {
		text-align: center;
	}

	.kotak {
		border: 1px solid rgba(0, 0, 0, 0.1);
		padding: 5px;
	}

	@media print {
		body * {
			visibility: hidden;
		}

		.kotak, .kotak * {
			visibility: visible;
		}

		.kotak {
			position: absolute;
			width: 100%;
			margin-top: 300px;
			transform: scale(2);
			left: 0;
			top: 0;
		}
	}
</style>


<div class="modal fade in" id="modal_hitung_pajak" tabindex="-1" aria-labelledby="myModalLabel35" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35">Hitung Pajak</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<?= form_open('GajiController/pphsave')?>
				<div class="modal-body" id="formhitungpajak">

				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal" value="Tutup">
					<input type="submit" class="btn btn-primary btn-bg-gradient-x-blue-cyan" name="save" value="Save" >
				</div>
			<?= form_close()?>
		</div>
	</div>
</div>


<div class="modal fade text-left" id="slip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header d-print-none">
				<h3 class="modal-title" id="myModalLabel35"> Lihat Slip Gaji</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body ">
				<div class="kotak d-print-block">
					<div class="row">
						<div class="col-12">
							<div class="tengah"><h3><b>SIPREG</b></h3></div>
							<div class="tengah">SISTEM PRESENSI DAN PENGGAJIAN</div>
							<hr>
							<div class="tengah"><b><u>SLIP GAJI KARYAWAN</u></b></div>
							<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<table>
								<tr>
									<td>Nama</td>
									<td>:</td>
									<td><span class="slip-nama"></span></td>
								</tr>
								<tr>
									<td>Jabatan</td>
									<td>:</td>
									<td><span id="slip-jabatan"></span></td>
								</tr>
								<tr>
									<td>Nomor HP</td>
									<td>:</td>
									<td><span id="slip-nohp"></span></td>
								</tr>
							</table>
						</div>
						<div class="col-6">
							<table>
								<tr>
									<td>Bulan</td>
									<td>:</td>
									<td><span class="slip-bulan"></span></td>
								</tr>
								<tr>
									<td>Jumlah Hari Masuk</td>
									<td>:</td>
									<td><span id="slip-hari"></span></td>
								</tr>
							</table>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-12">
							<p><b><u>Penghasilan</u></b></p>
							<table style="width: 100%">
								<tr>
									<td>Gaji Pokok</td>
									<td>:</td>
									<td>Rp. <span id="slip-gaji"></span></td>
								</tr>
								<tr>
									<td>Gaji Lembur</td>
									<td>:</td>
									<td>Rp. <span id="slip-lembur"></span></td>
								</tr>
								<tr>
									<td>Gaji Insentif</td>
									<td>:</td>
									<td>Rp. <span id="slip-insentif"></span></td>
								</tr>
								<tr>
									<td><b>Total</b></td>
									<td><b>:</b></td>
									<td><b>Rp. <span id="slip-total"></span></b></td>
								</tr>
							</table>
						</div>
						<div hidden="true" class="col-6">
							<p><b><u>Potongan</u></b></p>
							<table style="width: 100%">
								<tr>
									<td>Pinjaman</td>
									<td>:</td>
									<td><span class="slip-pinjam"></span></td>
								</tr>
								<tr>
									<td>PPh 21</td>
									<td>:</td>
									<td>Rp. <span id="slip-pph"></td>
								</tr>
								<tr>
									<td><b>Total</b></td>
									<td><b>:</b></td>
									<td><b><span class="slip-pinjam-total"></span></b></td>
								</tr>
							</table>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-12">
							<p class="tengah"><b>PENERIMAAN BERSIH = Rp. <span id="slip-bersih"></span></b></p>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<p class="tengah"><i>Terbilang : <span id="slip-terbilang"></span></i></p>
						</div>
					</div>
					<div hidden="true" class="row">
						<div class="col-12">
							<p class="tengah"><i>Sisa Pinjaman : Rp. <span id="slip-sisa-pinjam"></span></i></p>
						</div>
					</div>
					<div class="row">
						<div class="col-6 text-center" >
							<p><br></p>
							<p>Penerima</p>
							<br>
							<br>
							<br>
							<p><b><u><span class="slip-nama"></span></u></b></p>
						</div>
						<div class="col-6 text-center">
							<p>, <?= date_indo(date('Y-m-d')) ?></p>
							<p>Manajer</p>
							<br>
							<br>
							<br>
							<p><b><u></u></b></p>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer d-print-none">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
					   value="Tutup">
				<button onclick="window.print()" class="btn btn-primary btn-bg-gradient-x-purple-blue"><i
						class="fa fa-print"></i> Cetak
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal hapus -->
<div class="modal fade text-left" id="bayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel35"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="myModalLabel35"> Karyawan sudah menerima gaji ?</h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-footer">
				<input type="reset" class="btn btn-secondary btn-bg-gradient-x-red-pink" data-dismiss="modal"
					   value="Tutup">
				<div id="tombol-konfirmasi">

				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
<script>
	var root = window.location.origin + '/sipregcard/';

	function formatRupiah(angka, prefix){
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if(ribuan){
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function terbilang(s){
		var bilangan=s;
		var kalimat="";
		var angka   = new Array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
		var kata    = new Array('','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan');
		var tingkat = new Array('','Ribu','Juta','Milyar','Triliun');
		var panjang_bilangan = bilangan.length;

		/* pengujian panjang bilangan */
		if(panjang_bilangan > 15){
			kalimat = "Diluar Batas";
		}else{
			/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
			for(i = 1; i <= panjang_bilangan; i++) {
				angka[i] = bilangan.substr(-(i),1);
			}

			var i = 1;
			var j = 0;

			/* mulai proses iterasi terhadap array angka */
			while(i <= panjang_bilangan){
				subkalimat = "";
				kata1 = "";
				kata2 = "";
				kata3 = "";

				/* untuk Ratusan */
				if(angka[i+2] != "0"){
					if(angka[i+2] == "1"){
						kata1 = "Seratus";
					}else{
						kata1 = kata[angka[i+2]] + " Ratus";
					}
				}

				/* untuk Puluhan atau Belasan */
				if(angka[i+1] != "0"){
					if(angka[i+1] == "1"){
						if(angka[i] == "0"){
							kata2 = "Sepuluh";
						}else if(angka[i] == "1"){
							kata2 = "Sebelas";
						}else{
							kata2 = kata[angka[i]] + " Belas";
						}
					}else{
						kata2 = kata[angka[i+1]] + " Puluh";
					}
				}

				/* untuk Satuan */
				if (angka[i] != "0"){
					if (angka[i+1] != "1"){
						kata3 = kata[angka[i]];
					}
				}

				/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
				if ((angka[i] != "0") || (angka[i+1] != "0") || (angka[i+2] != "0")){
					subkalimat = kata1+" "+kata2+" "+kata3+" "+tingkat[j]+" ";
				}

				/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
				kalimat = subkalimat + kalimat;
				i = i + 3;
				j = j + 1;
			}

			/* mengganti Satu Ribu jadi Seribu jika diperlukan */
			if ((angka[5] == "0") && (angka[6] == "0")){
				kalimat = kalimat.replace("Satu Ribu","Seribu");
			}
		}
		return kalimat;
	}

	function date_indo(s) {
		var string = s;
		var split = string.split('-');
		var tahun = split[0];
		var bulan = split[1];
		var tanggal = split[2];
		var bulanArr = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

		return tanggal + ' ' + bulanArr[parseInt(bulan)-1] + ' ' + tahun;
	}

	$(document).ready(function(){
		$('#feedback').delay(3000).fadeOut('slow');

		$('.hitung_pajak').click(function () {
		var id = $(this).val();
		var getUrl = root + 'gajicontroller/getdataperhitungan/' + id;
		var html = '';
			$.ajax({
				url : getUrl,
				type : 'ajax',
				dataType : 'json',
				success: function (response) {
					if (response != null){
						$.map(response,function(obj){
							biayajabatan = parseFloat(obj.gaji_total) * 5 / 100; 
							idkaryawan = obj.karyawan_id;
							html += '' +
									'<div class="row">'+
										'<div class="col-md-6">'+
											'<input type="hidden" value="'+idkaryawan+'" name="id" id="idkaryawan">' +
											'<input type="hidden" value="'+obj.karyawan_status+'" id="status_karyawan">' +
											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="lbl_nama_karyawan">Nama Karyawan</label>' +
											'<input readonly type="text" class="form-control" name="nama_karyawan" id="nama_karyawan" value="'+idkaryawan+' | '+obj.karyawan_nama+'" placeholder="Jabatan">' +
											'</fieldset>' +

											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="lbl_jabatan">Jabatan</label>' +
											'<input readonly type="text" class="form-control" name="jabatan_karyawan" id="jabatan_karyawan" value="'+obj.karyawan_jabatan_id+' | '+obj.jabatan_nama+'" placeholder="Jumlah gaji">' +
											'</fieldset>'+

											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="lbl_gaji_poko">Gaji Pokok</label>' +
											'<input readonly type="text" class="form-control" name="gaji_pokok" id="gaji_pokok" value="'+obj.gaji_total+'" placeholder="Tunjangan Jabatan" autocomplete="off" required>' +
											'</fieldset>'+

											'<label for="tunkon">Pengurang</label>' +
											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Biaya Jabatan</label>' +
											'<input type="text" class="form-control" name="biaya_jabatan" id="biaya_jabatan" value="'+biayajabatan+'" placeholder="Biaya Jabatan" autocomplete="off" required>' +
											'</fieldset>'+

											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Iuran Pensiun</label>' +
											'<input type="text" class="form-control" name="iuranpensiun" id="iuranpensiun" onkeyup="hitungptkp()" value="0" placeholder="Iuran Pensiun" autocomplete="off" required>' +
											'</fieldset>'+
											
											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Total Pengurang</label>' +
											'<input readonly type="text" class="form-control" name="totalpengurang" id="totalpengurang" value="0" placeholder="Total pengurang" autocomplete="off" required>' +
											'</fieldset>'+
										'</div>'+
										'<div class="col-md-6">'+
											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Penghasilan Perbualan</label>' +
											'<input readonly type="text" class="form-control" name="netoperbulan" id="netoperbulan" value="0" placeholder="Netto Perbulan" autocomplete="off" required>' +
											'</fieldset>'+
											
											'<fieldset  hidden="Hidden" class="form-group floating-label-form-group">' +
											'<label for="tunkon">Penghasilan Pertahun</label>' +
											'<input readonly type="text" class="form-control" name="netoperpertahun" id="netoperpertahun" value="0" placeholder="Netto Pertahun" autocomplete="off" required>' +
											'</fieldset>'+
											
											'<label for="tunkon">PTKP</label>' +
											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Jumlah Anak</label>' +
											'<input type="text" class="form-control" name="anak" id="anak" onkeyup="hitungptkp()" value="0" placeholder="Anak" autocomplete="off" required>' +
											'</fieldset>'+

											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Total PTKP</label>' +
											'<input type="text" class="form-control" name="totalptkp" id="totalptkp" value="0" placeholder="Total PTKP" autocomplete="off" required>' +
											'</fieldset>'+
											
											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Total Penghasilan Kenapajak</label>' +
											'<input type="text" class="form-control" name="tkp" id="tkp" value="0" placeholder="Total Penghasilan Kenapajak" autocomplete="off" required>' +
											'</fieldset>'+

											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Kenapajak Pertahun</label>' +
											'<input type="text" class="form-control" name="kenatahun" id="kenatahun" value="0" placeholder="Total Penghasilan Kenapajak" autocomplete="off" required>' +
											'</fieldset>'+

											'<fieldset class="form-group floating-label-form-group">' +
											'<label for="tunkon">Kenapajak Perbulan</label>' +
											'<input type="text" class="form-control" name="kenabulan" id="kenabulan" value="0" placeholder="Total Penghasilan Kenapajak" autocomplete="off" required>' +
											'</fieldset>'+
										'</div>'+


									'</div>'

									;

							// console.log(html);
							$('#formhitungpajak').html(html);

						});
					}
				},
				error: function (response) {
					console.log(response.status + 'error');
				}
			});
		});

		$('.gaji-lihat').click(function (e) {
			e.preventDefault();
			var id = $(this).val();
			var getUrl = root + 'gajicontroller/lihat/' + id;
			var total = 0;
			$.ajax({
				url : getUrl,
				type : 'ajax',
				dataType : 'json',
				success: function (response) {
					if (response != null){
						$('#gaji_lihat_nama').val(response.karyawan_nama);
						$('#gaji_lihat_jabatan').val(response.jabatan_nama);
						$('#gaji_lihat_tg').val(response.karyawan_tanggal_gabung);
						$('#gaji_lihat_lembur').val(formatRupiah(response.gaji_lembur,'Rp. '));
						$('#gaji_lihat_gaji').val(formatRupiah(response.gaji_total,'Rp. '));
						total = parseInt(response.gaji_lembur) + parseInt(response.gaji_total);
						$('#gaji_lihat_total').val(formatRupiah(total.toString(),'Rp. '));
						bersih = total - parseInt(response.gaji_bayar_pinjaman);
						$('#gaji_lihat_bersih').val(formatRupiah(bersih.toString(),'Rp. '));
						$('#gaji_lihat_bulan').val(response.gaji_bulan_ke);
					}
				},
				error: function (response) {
					console.log(response.status + 'error');
				}
			});
		});

		$('.gaji-slip').click(function (e) {
			e.preventDefault();
			var id = $(this).val();
			var getUrl = root + 'gajicontroller/lihat/' + id;
			var total = 0;
			$.ajax({
				url : getUrl,
				type : 'ajax',
				dataType : 'json',
				success: function (response) {
					if (response != null){
						var jumlahHari = ['31','28','31','30','31','30','31','31','30','31','30','31'];
						var insentif = 0;
						var myStr2 = response.gaji_tanggal;
						var strArray2 = myStr2.split("-");
						var bulan2 = strArray2[1];
						pphbulan = parseFloat(response.pph_kenabulan);

						$('.slip-nama').html(response.karyawan_nama);
						$('#slip-jabatan').html(response.jabatan_nama);
						$('#slip-nohp').html(response.karyawan_nomor_hp);
						$('#slip-lembur').html(formatRupiah(response.gaji_lembur));
						
						if (jumlahHari[parseInt(bulan2)] == response.jumlah_hari.jumlah_hari ) {
							insentif = response.jabatan_gaji;
						} else {
							insentif = 0;
						}

						$('slip-insentif').html(formatRupiah(insentif.toString(), 'Rp. '));

						$('#slip-gaji').html(formatRupiah(response.gaji_total));
						total = parseInt(response.gaji_lembur) + parseInt(response.gaji_total) + parseInt(insentif) ;
						$('#slip-total').html(formatRupiah(total.toString()));

						var myStr = date_indo(response.gaji_tanggal);
						var strArray = myStr.split(" ");
						var bulan = strArray[1];

						$('.slip-bulan').html(bulan);
						$('#slip-hari').html(jumlahHari[parseInt(bulan2)]);

						var getUrl2 = root + 'gaji/pinjam/' + id;
						$.ajax({
							url : getUrl2,
							type : 'ajax',
							dataType : 'json',
							success : function (response2) {
								if (response2 != null){
									if ((parseInt(response2.pinjam_jumlah) - parseInt(response2.pinjam_bayar)) > 500000){
										$('.slip-pinjam').html(formatRupiah('500000'));
										bersih = total - 500000;
										sisa = (parseInt(response2.pinjam_jumlah - response2.pinjam_bayar)) - 500000;
										total = parseInt(response2.pinjam_jumlah - response2.pinjam_bayar)+pphbulan; 


										$('#slip-bersih').html(formatRupiah(bersih.toString()));
										$('#slip-pph').html(formatRupiah(pphbulan.toString()));
										$('#slip-sisa-pinjam').html(formatRupiah(sisa.toString()));
										$('#slip-terbilang').html(terbilang(bersih.toString()) + 'Rupiah');

										$('.slip-pinjam-total').html("Rp. "+formatRupiah(total.toString()));
									} else {
										pinjam = parseInt(response2.pinjam_jumlah - response2.pinjam_bayar);
										total = parseInt(response2.pinjam_jumlah - response2.pinjam_bayar)+pphbulan; 
										
										$('.slip-pinjam').html(formatRupiah(pinjam.toString()));
										bersih = total - pinjam;
										sisa = (parseInt(response2.pinjam_jumlah) - parseInt(response2.pinjam_bayar)) - pinjam;
										$('#slip-pph').html(formatRupiah(pphbulan.toString()));
										$('#slip-bersih').html(formatRupiah(bersih.toString()));
										$('#slip-sisa-pinjam').html(formatRupiah(sisa.toString()));
										$('#slip-terbilang').html(terbilang(bersih.toString()) + 'Rupiah');
										
										$('.slip-pinjam-total').html("Rp. "+formatRupiah(total.toString()));
									}
								}
								else {
									$('.slip-pinjam').html("Rp. 0");
									$('.slip-pinjam-total').html("Rp. "+formatRupiah(pphbulan.toString()));
									sisa = 0;
									$('#slip-pph').html(formatRupiah(pphbulan.toString()));
									$('#slip-bersih').html(formatRupiah(total.toString()));
									$('#slip-sisa-pinjam').html(formatRupiah(sisa.toString()));
									$('#slip-terbilang').html(terbilang(total.toString()) + 'Rupiah');
								}
							},
							error: function (response2) {
								console.log(response2.status + 'error');
							}
						});
						console.log(response);
					}
				},
				error: function (response) {
					console.log(response.status + 'error');
				}
			});
		});

		$('.gaji-bayar').click(function () {
			var id = $(this).val();
			var html = '' +
				'<a href="'+root+'gajicontroller/bayar/'+id+'" class="btn btn-danger btn-bg-gradient-x-blue-cyan">Konfirmasi</a>';
			$('#tombol-konfirmasi').html(html);
		});
	});
</script>

