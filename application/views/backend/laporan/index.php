<style type="text/css">
	.kotak {
		padding: 5px;
	}

	@page {
		size: A4;
		margin: 0;
	}

	@media print {
		body * {
			visibility: hidden;
		}

		.kotak, .kotak * {
			visibility: visible;
		}

		.kotak {
			z-index: 2;
			position: absolute;
			width: 100%;
			top: 0;
			left: 0;
		}
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="card box-shadow-2">
			<div class="card-header">
				<h1 style="text-align: center">Laporan</h1>
			</div>
			<div class="card-body">
				<div>
					<fieldset class="form-group floating-label-form-group">
						<div class="row">
							<div class="col-4">
								<label for="laporan-tanggal">Tanggal</label>
								<select name="tanggal" id="laporan-tanggal" class="form-control">
									<option value="-">Pilih Tanggal</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
								</select>
							</div>
							<div class="col-4">
								<label for="laporan-bulan">Bulan</label>
								<select name="bulan" id="laporan-bulan" class="form-control">
									<option value="-">Pilih Bulan</option>
									<option value="01">Januari</option>
									<option value="02">Februari</option>
									<option value="03">Maret</option>
									<option value="04">April</option>
									<option value="05">Mei</option>
									<option value="06">Juni</option>
									<option value="07">Juli</option>
									<option value="08">Agustus</option>
									<option value="09">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
							</div>
							<div class="col-4">
								<label for="laporan-tahun">Tahun</label>
								<input type="text" id="laporan-tahun" class="form-control" value="<?=date('Y')?>">
							</div>
							<div class="col-12 mt-3">
								<button class="btn btn-primary btn-block btn-bg-gradient-x-blue-cyan" id="laporan-btn-lihat">Lihat</button>
							</div>
						</div>
					</fieldset>
				</div>
				<hr>
				<div id="laporan" class="kotak">

				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
<script>
	var root = window.location.origin + '/sipregcard/';

	function formatRupiah(angka, prefix){
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if(ribuan){
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	function date_indo(s) {
		var string = s;
		var split = string.split('-');
		var tahun = split[0];
		var bulan = split[1];
		var tanggal = split[2];
		var bulanArr = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

		return tanggal + ' ' + bulanArr[parseInt(bulan)-1] + ' ' + tahun;
	}

	$(document).ready(function(){
		$('#feedback').delay(3000).fadeOut('slow');

		$('#laporan-btn-lihat').click(function () {
		var tahun = $('#laporan-tahun').val();
		var bulan = $('#laporan-bulan').val();
		var tanggal = $('#laporan-tanggal').val();
		var params = '';

		if (tanggal == '-') {
			params = tahun +'-'+bulan;
		} else if (tanggal == '-' && bulan == '-') {
			params = tahun;
		} else if (tanggal != '-' && bulan != '-') {
			params = tahun+'-'+bulan+'-'+tanggal;
		}

		var getUrl = root + 'laporancontroller/lihat/' + params;
		// var getUrl = root + 'laporan/lihat/' + tahun +'/'+ bulan +'/'+ tanggal;
		var bulanArr = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		var html = '';
			$.ajax({
				url : getUrl,
				type : 'get',
				dataType : 'json',
				success: function (response) {
					var jumlahHari = ['31','28','31','30','31','30','31','31','30','31','30','31'];

					if (response != null){
						html += '' +						
							'<h2 style="text-align: center">SIPREG SYSTEM</h2>' +
							'<div class="d-print-none float-right">' +
							'<button onclick="window.print()" class="btn btn-sm btn-primary btn-bg-gradient-x-purple-blue"><i class="fa fa-user"></i> Cetak' +
							'</button>' +
							'</div>';
						html += '' +
							'<p style="text-align: center">Laporan Bulan '+bulanArr[parseInt(bulan)-1]+' '+tahun+'</p>' +
							'<table class="table table-bordered">' +
							'<thead style="text-align: center">' +
							'<tr>' +
							'<th>No</th>' +
							'<th>Nama Karyawan</th>' +
							'<th>Jabatan</th>' +
							'<th>Tanggal</th>' +
							'<th>Lemburan</th>' +
							'<th>Insentif</th>' +
							'<th>Gaji</th>' +
							'<th>Gaji Bersih</th>' +
							'</tr>' +
							'</thead>' +
							'<tbody>';
						var no = 1;
						var total = 0;
						var kotor = 0;
						var pinjaman = 0;
						for (var i = 0; i < response.length; i++){	
							var myStr2 = response[i].gaji_tanggal;
							var strArray2 = myStr2.split("-");
							var bulan2 = strArray2[1];
							
							var insentif = '0';
							// if (jumlahHari[parseInt(bulan2)] == response[i].) {
							// 	insentif = response[i].jabatan_gaji;
							// } else{
							// 	insentif = '0';
							// }

							var totalGajiBersih = parseInt(response[i].gaji_lembur) + parseInt(response[i].gaji_total) + parseInt(insentif);

							html += '' +
								'<tr>' +
								'<td>'+no+'</td>' +
								'<td>'+response[i].karyawan_nama+'</td>' +
								'<td>'+response[i].jabatan_nama+'</td>' +
								'<td>'+date_indo(response[i].gaji_tanggal)+'</td>' +
								'<td style="text-align: right"> Rp. '+formatRupiah((parseInt(response[i].gaji_lembur)).toString())+'</td>' +
								'<td style="text-align: right"> Rp. '+formatRupiah((parseInt(insentif)).toString())+'</td>' +
								'<td style="text-align: right"> Rp. '+formatRupiah((parseInt(response[i].gaji_total)).toString())+'</td>' +
								'<td style="text-align: right"> Rp. '+formatRupiah(totalGajiBersih.toString())+'</td>' +
								'</tr>';
							total = total + (parseInt(response[i].gaji_lembur) + parseInt(response[i].gaji_total));
							kotor = kotor + (parseInt(response[i].gaji_lembur) + parseInt(response[i].gaji_total));
							pinjaman = pinjaman + parseInt(response[i].gaji_bayar_pinjaman);
							no++;
						}
						var d = new Date();
						html += '' +
							'</tbody>' +
							'<tfoot>' +
							'<tr>' +
							'<td colspan="6" style="text-align: center"><b>Total</b></td>' +
							'<td style="text-align: right"> <b>Rp.'+formatRupiah(kotor.toString())+'</b></td>' +
							'<td style="text-align: right"> <b>Rp.'+formatRupiah(total.toString())+'</b></td>' +
							'</tr>' +
							'</tfoot>' +
							'</table>' +
							'<div class="row">' +
							'<div class="col-8"></div>' +
							'<div class="col-4 text-center">' +
							'<p>Bandung, '+date_indo(d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate())+'</p>' +
							'<p>Manajer</p>' +
							'<br>' +
							'<br>' +
							'<br>' +
							'<p><b><u>Manajer</u></b></p>' +
							'</div>' +
							'</div>';
						$('#laporan').html(html);
					}
				},
				error : function (response) {
					console.log(response.status + 'error');
				}
			})
		});
	});
</script>