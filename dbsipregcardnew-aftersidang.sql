-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for dbsipregcard
CREATE DATABASE IF NOT EXISTS `dbsipregcard` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `dbsipregcard`;

-- Dumping structure for table dbsipregcard.t_absen
CREATE TABLE IF NOT EXISTS `t_absen` (
  `absen_id` varchar(20) NOT NULL,
  `absen_karyawan_id` varchar(20) NOT NULL,
  `absen_ke` smallint(6) NOT NULL DEFAULT 0,
  `absen_hari` varchar(10) NOT NULL,
  `absen_in` varchar(30) NOT NULL,
  `absen_out` varchar(30) NOT NULL,
  `absen_status` enum('lembur','normal') NOT NULL DEFAULT 'normal',
  `absen_izin_masuk` varchar(30) NOT NULL DEFAULT '',
  `absen_izin_keluar` varchar(30) NOT NULL DEFAULT '',
  `absen_lembur` varchar(30) NOT NULL DEFAULT '',
  `absen_date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`absen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dbsipregcard.t_absen: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_absen` DISABLE KEYS */;
REPLACE INTO `t_absen` (`absen_id`, `absen_karyawan_id`, `absen_ke`, `absen_hari`, `absen_in`, `absen_out`, `absen_status`, `absen_izin_masuk`, `absen_izin_keluar`, `absen_lembur`, `absen_date_created`) VALUES
	('ABS-18959', '0639866529', 1, 'Kamis', '2022-07-28 09:35:59', '2022-07-28 21:53:01', 'normal', '2022-07-28 21:53:01', '2022-07-28 21:43:57', '', '2022-07-28 21:36:00'),
	('ABS-18970', '0639866529', 2, 'Kamis', '2022-07-28 09:35:59', '2022-07-28 21:53:01', 'lembur', '', '', '2022-07-31 20:00:00', '2022-07-29 21:36:00');
/*!40000 ALTER TABLE `t_absen` ENABLE KEYS */;

-- Dumping structure for table dbsipregcard.t_gaji
CREATE TABLE IF NOT EXISTS `t_gaji` (
  `gaji_id` varchar(20) NOT NULL,
  `gaji_karyawan_id` varchar(20) NOT NULL,
  `gaji_lembur` double DEFAULT NULL,
  `gaji_total` double NOT NULL DEFAULT 0,
  `gaji_bayar_pinjaman` double NOT NULL DEFAULT 0,
  `gaji_tanggal` date DEFAULT NULL,
  `gaji_bulan_ke` double DEFAULT NULL,
  `gaji_status` enum('sudah','belum') NOT NULL DEFAULT 'belum',
  `gaji_date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`gaji_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dbsipregcard.t_gaji: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_gaji` DISABLE KEYS */;
REPLACE INTO `t_gaji` (`gaji_id`, `gaji_karyawan_id`, `gaji_lembur`, `gaji_total`, `gaji_bayar_pinjaman`, `gaji_tanggal`, `gaji_bulan_ke`, `gaji_status`, `gaji_date_created`) VALUES
	('GJI-18959', '0639866529', 200000, 3100000, 0, '2022-07-28', 1, 'sudah', '2022-07-28 21:35:59');
/*!40000 ALTER TABLE `t_gaji` ENABLE KEYS */;

-- Dumping structure for table dbsipregcard.t_jabatan
CREATE TABLE IF NOT EXISTS `t_jabatan` (
  `jabatan_id` varchar(20) NOT NULL,
  `jabatan_nama` varchar(255) DEFAULT '0',
  `jabatan_gaji` double(22,0) DEFAULT 0,
  `jabatan_tunjab` double(22,0) DEFAULT 0,
  `jabatan_tunkon` double(22,0) DEFAULT 0,
  `jabatan_tunlan` double(22,0) DEFAULT 0,
  `jabatan_date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dbsipregcard.t_jabatan: ~5 rows (approximately)
/*!40000 ALTER TABLE `t_jabatan` DISABLE KEYS */;
REPLACE INTO `t_jabatan` (`jabatan_id`, `jabatan_nama`, `jabatan_gaji`, `jabatan_tunjab`, `jabatan_tunkon`, `jabatan_tunlan`, `jabatan_date_created`) VALUES
	('JAB-40074', 'Office', 80000, 20000, 15000, 300000, '2021-01-10 07:54:34'),
	('JAB-66949', 'IT', 150000, 10000, 10000, 300000, '2021-01-09 18:21:22'),
	('JAB-91321', 'Admin', 100000, 10000, 10000, 300000, '2021-01-09 18:22:01'),
	('JAB-91343', 'Staf', 120000, 10000, 10000, 300000, '2021-01-09 18:22:23');
/*!40000 ALTER TABLE `t_jabatan` ENABLE KEYS */;

-- Dumping structure for table dbsipregcard.t_karyawan
CREATE TABLE IF NOT EXISTS `t_karyawan` (
  `karyawan_id` varchar(20) NOT NULL,
  `karyawan_jabatan_id` varchar(20) DEFAULT NULL,
  `karyawan_nama` varchar(255) DEFAULT NULL,
  `karyawan_tempat_lahir` varchar(255) DEFAULT NULL,
  `karyawan_tanggal_lahir` date DEFAULT NULL,
  `karyawan_alamat` text DEFAULT NULL,
  `karyawan_tanggal_gabung` date DEFAULT NULL,
  `karyawan_status` varchar(50) DEFAULT NULL,
  `karyawan_nomor_hp` varchar(20) DEFAULT NULL,
  `karyawan_no_rekening` varchar(30) DEFAULT NULL,
  `karyawan_date_created` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`karyawan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dbsipregcard.t_karyawan: ~1 rows (approximately)
/*!40000 ALTER TABLE `t_karyawan` DISABLE KEYS */;
REPLACE INTO `t_karyawan` (`karyawan_id`, `karyawan_jabatan_id`, `karyawan_nama`, `karyawan_tempat_lahir`, `karyawan_tanggal_lahir`, `karyawan_alamat`, `karyawan_tanggal_gabung`, `karyawan_status`, `karyawan_nomor_hp`, `karyawan_no_rekening`, `karyawan_date_created`) VALUES
	('0639866529', 'JAB-91321', 'Asep Christoper', 'Bandung', '1987-02-20', 'Bandung', '2022-06-26', 'LAJANG', '08799688999', '08096888', '2022-07-28 21:40:08');
/*!40000 ALTER TABLE `t_karyawan` ENABLE KEYS */;

-- Dumping structure for table dbsipregcard.t_pengguna
CREATE TABLE IF NOT EXISTS `t_pengguna` (
  `pengguna_id` int(20) NOT NULL AUTO_INCREMENT,
  `pengguna_username` varchar(255) DEFAULT NULL,
  `pengguna_password` varchar(255) DEFAULT NULL,
  `pengguna_nama` varchar(255) DEFAULT NULL,
  `pengguna_foto` text DEFAULT NULL,
  `pengguna_hak_akses` enum('manajer','owner') DEFAULT NULL,
  `pengguna_date_created` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`pengguna_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table dbsipregcard.t_pengguna: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pengguna` DISABLE KEYS */;
REPLACE INTO `t_pengguna` (`pengguna_id`, `pengguna_username`, `pengguna_password`, `pengguna_nama`, `pengguna_foto`, `pengguna_hak_akses`, `pengguna_date_created`) VALUES
	(1, 'manajer', '21232f297a57a5a743894a0e4a801fc3', 'Yayan Jatnika', NULL, 'manajer', '2019-07-15 12:27:55');
/*!40000 ALTER TABLE `t_pengguna` ENABLE KEYS */;

-- Dumping structure for table dbsipregcard.t_pinjam
CREATE TABLE IF NOT EXISTS `t_pinjam` (
  `pinjam_id` varchar(20) NOT NULL,
  `pinjam_karyawan_id` varchar(20) NOT NULL,
  `pinjam_jumlah` int(20) NOT NULL,
  `pinjam_bayar` int(20) NOT NULL DEFAULT 0,
  `pinjam_date_created` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`pinjam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table dbsipregcard.t_pinjam: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_pinjam` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_pinjam` ENABLE KEYS */;

-- Dumping structure for table dbsipregcard.t_setting_absen
CREATE TABLE IF NOT EXISTS `t_setting_absen` (
  `setting_absen_id` int(10) NOT NULL AUTO_INCREMENT,
  `jam_masuk` varchar(50) NOT NULL DEFAULT '0',
  `jam_keluar` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`setting_absen_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table dbsipregcard.t_setting_absen: ~1 rows (approximately)
/*!40000 ALTER TABLE `t_setting_absen` DISABLE KEYS */;
REPLACE INTO `t_setting_absen` (`setting_absen_id`, `jam_masuk`, `jam_keluar`) VALUES
	(1, '08:00:00', '17:00:00');
/*!40000 ALTER TABLE `t_setting_absen` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
